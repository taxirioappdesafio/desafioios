## Desafio Desenvolvedor Mobile iOS
Iplanrio - Empresa Municipal de Informática do RJ

### Avaliação
Esta etapa do processo seletivo consiste no desenvolvimento de um aplicativo iOS, com a finalidade de avaliar as habilidades dos candidatos de vários níveis.

Seu projeto será avaliado de acordo com os seguintes critérios:
- Sua aplicação preenche os requerimentos básicos?
- Você seguiu as instruções de envio do desafio?

### Prazo de Entrega
Após o recebimento do desafio, pedimos que a atividade seja realizada dentro do período de 7 dias úteis.

### Instruções de Entrega
- O candidato deverá criar o projeto em um repositório privado no Bitbucket ou no GitHub;
- Conceder acesso de leitura ao nosso usuário: **selecao-taxirio**

### Requisitos
O candidato deve criar um aplicativo para o sistema operacional iOS para solicitação de corridas de táxi.

##### Parte 1 - Validar Usuário:
- Criar tela de login com os campos:
  - email;
  - senha.

- Utilizar a API para validação:
  - http://private-434d2-desafiomobileios.apiary-mock.com/signin
  - POST;
  - (Headers: “Content-Type : application/json”)
  - {
      "email": "passageiro@email.com",
      "password": "123456"
    }

##### Parte 2 - Home:
- O aplicativo deve exibir inicialmente uma tela com:
  - o mapa carregado;
  - campo de texto para informar o endereço de origem;
  - campo de texto para informar o endereço de destino;
  - botão avançar;
  - a localização do usuário deve ser marcada por um Pin;
  - campo de origem preenchido com a sua localização.


- Traçar Rota:
  - Ao preencher o campo de destino deverá ser traçado uma rota entre a origem e o destino.


- Clicar botão "Avançar":
    - armazenar os dados de origem e destino e armazenar em um banco de dados local;
    - Exibir tela de histórico de corridas

##### Parte 3 - Histórico de Corridas:
  - Exibir tela de histórico de corridas contendo e em cada registro de uma tabela:
    * origem;
    * destino;
    * data da solicitação;
    * tempo estimado da corrida.


### Requisitos técnicos obrigatórios
- desenvolver com a linguagem de programação Swift 4;
- suportar versão mínima do iOS 10.* ;
- usar Storyboard e Autolayout;
- fazer um app compatível e adaptável aos modelos: iPhone X, 8 Plus, 8, 7 Plus, 7, 6s Plus, 6s e SE;
- usar gestão de dependências no projeto. Ex: Cocoapods;
- usar um Framework para Comunicação com API. Ex: Moya, Alamofire;
- persistir os dados localmente usando Core Data ou API Realm;
- usar API do Google Maps: Maps SDK for iOS, para Mapa, Geocoder, Directions (Rota);

### Requisitos técnicos desejáveis
- usar um arquivo .gitignore no seu repositório;
- usar protocolo Codable para o mapeamento json -> Objeto;
- criar testes unitários. Ex: XCTests / Specta + Expecta;
- criar testes funcionais. Ex: KIF;  
